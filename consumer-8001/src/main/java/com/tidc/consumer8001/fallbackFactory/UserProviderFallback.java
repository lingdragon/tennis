package com.tidc.consumer8001.fallbackFactory;

import com.tidc.consumer8001.api.UserProviderApi;
import com.tidc.consumer8001.ov.UserOV;
import com.tidc.consumer8001.pojo.User;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @ClassNmae UserProviderFallback
 * @Description TODO
 * @Author 冯涛滔
 **/
@Component
public class UserProviderFallback implements FallbackFactory<UserProviderApi> {
	@Override
	public UserProviderApi create(Throwable throwable) {
		return new UserProviderApi() {
			@Override
			public String test(int id) {
				return "test报错了";
			}

			@Override
			public UserOV register(User user) {
				return null;
			}
		};
	}
}
