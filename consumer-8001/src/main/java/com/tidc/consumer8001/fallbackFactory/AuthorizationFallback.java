package com.tidc.consumer8001.fallbackFactory;

import com.tidc.consumer8001.api.AuthorizationApi;
import feign.hystrix.FallbackFactory;

/**
 * @ClassNmae AuthorizationFallback
 * @Description TODO
 * @Author 冯涛滔
 **/
public class AuthorizationFallback implements FallbackFactory<AuthorizationApi> {
	@Override
	public AuthorizationApi create(Throwable throwable) {
		new AuthorizationApi() {
			@Override
			public Object login(String username, String password, String client_id, String client_secret, String grant_type) {
				return "用户名密码错误";
			}

			@Override
			public Object logout(String access_token) {
				return "退出失败";
			}
		};
		return null;
	}
}
