package com.tidc.consumer8001.api;

import com.tidc.consumer8001.fallbackFactory.UserProviderFallback;
import com.tidc.consumer8001.ov.UserOV;
import com.tidc.consumer8001.pojo.User;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassNmae UserProviderApi
 * @Description TODO
 * @Author 冯涛滔
 **/
@FeignClient(name = "USERPROVIDER",fallbackFactory = UserProviderFallback.class)
public interface UserProviderApi {
	@RequestMapping(value= "/test/{id}",method = RequestMethod.GET)
	public String test(@PathVariable("id") int id);
	@RequestMapping(value = "/user/register", method = RequestMethod.POST)
	@Headers("Content-Type: application/json")
	public UserOV register(@RequestBody User user);

}
