package com.tidc.consumer8001.api;

import com.tidc.consumer8001.fallbackFactory.AuthorizationFallback;
import com.tidc.consumer8001.fallbackFactory.UserProviderFallback;
import com.tidc.consumer8001.ov.UserOV;
import com.tidc.consumer8001.pojo.User;
import feign.Headers;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ClassNmae AuthorizationApi
 * @Description TODO
 * @Author 冯涛滔
 **/
@FeignClient(name = "AUTHORIZATION",fallbackFactory = AuthorizationFallback.class)
public interface AuthorizationApi {
	@RequestMapping(value = "/oauth/token", method = RequestMethod.POST)
	@Headers("Content-Type: application/json")
	public Object login(@RequestParam("username") String username, @RequestParam("password")String password, @RequestParam("client_id")String client_id, @RequestParam("client_secret")String client_secret,@RequestParam("grant_type") String grant_type);
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@Headers("Content-Type: application/json")
	public Object logout( @Param("token") String token);
}
