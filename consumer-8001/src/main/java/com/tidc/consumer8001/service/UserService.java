package com.tidc.consumer8001.service;

import com.tidc.consumer8001.api.AuthorizationApi;
import com.tidc.consumer8001.api.UserProviderApi;
import com.tidc.consumer8001.ov.UserOV;
import com.tidc.consumer8001.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassNmae UserService
 * @Description TODO
 * @Author 冯涛滔
 **/
@Service
public class UserService {
	@Autowired
	private UserProviderApi userProviderApi;
	@Autowired
	private AuthorizationApi authorizationApi;
	public UserOV register(User user){
		return userProviderApi.register(user);
	}
	public Object login(String username,String password,String client_id,String client_secret,String grant_type){
		return authorizationApi.login(username, password, client_id, client_secret, grant_type);
	}
	public Object logout(String token){
		return authorizationApi.logout(token);
	}
}
