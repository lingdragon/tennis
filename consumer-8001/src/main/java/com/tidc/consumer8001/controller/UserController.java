package com.tidc.consumer8001.controller;

import com.tidc.consumer8001.ov.UserOV;
import com.tidc.consumer8001.pojo.User;
import com.tidc.consumer8001.service.UserService;
import feign.Param;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassNmae UserController
 * @Description TODO
 * @Author 冯涛滔
 **/
@CrossOrigin(origins = "*",maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;
	@ApiOperation("注册账号")
	@PostMapping("/register")
	public UserOV teamId( @RequestBody  User user) {
		System.out.println(user);
		return userService.register(user);
	}
	@ApiOperation("用户登录")
	@PostMapping("/login")
//	@RequestParam("client_id")String client_id, @RequestParam("client_secret")String client_secret,@RequestParam("grant_type") String grant_type
	public Object login(@RequestParam("username") String username, @RequestParam("password")String password){
		System.out.println(username);
		System.out.println("test");
		return userService.login(username,password,"TIDC","computer","password");
	}
	@ApiOperation("用户退出")
	@GetMapping ("/logout")
	@PreAuthorize(value = "hasRole('ROLE_USER') or hasAuthority('ROLE_ADMIN')")

	public Object logout(@Param("token")  String token){
		System.out.println(token);
		return userService.logout(token);
	}

}
