package com.tidc.consumer8001.controller;

import com.tidc.consumer8001.api.UserProviderApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassNmae testController
 * @Description TODO
 * @Author 冯涛滔
 **/
@CrossOrigin
@RestController
public class testController {
	@Autowired
	private UserProviderApi userProviderApi;
	@GetMapping("/test/{id}")
	@PreAuthorize(value = "hasRole('ROLE_USER') or hasAuthority('ROLE_ADMIN')")
	public String test(@PathVariable("id") int id, Authentication authentication){
		System.out.println(authentication);
		return userProviderApi.test(id);
	}

}
