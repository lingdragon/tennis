package com.tidc.userprovider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tidc.userprovider.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @ClassNmae UserMapper
 * @Description TODO
 * @Author 冯涛滔
 **/
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
