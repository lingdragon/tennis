package com.tidc.userprovider.controller;

import com.tidc.userprovider.ov.UserOV;
import com.tidc.userprovider.pojo.User;
import com.tidc.userprovider.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassNmae UserController
 * @Description TODO
 * @Author 冯涛滔
 **/
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/user")
@RestController
public class UserController {
	@Autowired
	private UserService userService;
	@ApiOperation("注册账号")
	@PostMapping("/register")
	public UserOV teamId(@RequestBody User user) {
		return userService.register(user);
	}
}
