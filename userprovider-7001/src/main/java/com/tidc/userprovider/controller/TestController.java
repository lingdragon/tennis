package com.tidc.userprovider.controller;

import com.tidc.userprovider.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassNmae TestController
 * @Description TODO
 * @Author 冯涛滔
 **/
@RestController
public class TestController {
	@Autowired
	private UserService userService;
	@GetMapping("/test/{id}")
	public String test(@PathVariable("id") int id){
		if(id==2)throw new RuntimeException();
		return id+"test"+7001;
	}
}
