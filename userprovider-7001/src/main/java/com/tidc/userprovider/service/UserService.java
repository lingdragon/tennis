package com.tidc.userprovider.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tidc.userprovider.mapper.UserMapper;
import com.tidc.userprovider.ov.UserOV;
import com.tidc.userprovider.pojo.User;
import com.tidc.userprovider.utils.ApplicationContextProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @ClassNmae UserService
 * @Description TODO
 * @Author 冯涛滔
 **/
@Service
public class UserService {
	@Autowired
	private ApplicationContextProvider ac;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private UserMapper userMapper;
	public UserOV register(User user){
		UserOV userOV = ac.getBean(UserOV.class);
		System.out.println(user);
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("email",user.getEmail());
		User user2 = userMapper.selectOne(queryWrapper);
		if (user2 == null) {
		 	user.setPassword(passwordEncoder.encode(user.getPassword()));
			userMapper.insert(user);
			return userOV.setCode(200).setMessage("注册成功");
		}
		 return userOV.setCode(401).setMessage("email重复");
	}

}
