package com.tidc.authorization.exception;

import com.tidc.authorization.ov.RestfulUserVOResultBuilder;
import com.tidc.authorization.ov.UserOV;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @ClassNmae GlobalExceptionHandler
 * @Description TODO
 * @Author 14631
 **/
@ControllerAdvice
public class GlobalExceptionHandler implements RestfulUserVOResultBuilder {
	/**
	 * 处理，实际为捕捉全局异常
	 *
	 * @param exception 全局异常
	 * @return 具体异常信息 后面那个是500报错内容是服务器内部异常
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<UserOV> handlerException(Exception exception) {
		//当未知异常发生时，将信息弹出堆栈
		exception.printStackTrace();
		return new ResponseEntity<>(faled(500,exception.getMessage()+"这个是全局异常难搞"), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	/**
	 * 处理 http消息不可读异常
	 *
	 * @param httpMessageNotReadableException http消息不可读异常
	 * @return 具体异常信息
	 */
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<UserOV> handlerHttpMessageNotReadableException(HttpMessageNotReadableException httpMessageNotReadableException) {
		return new ResponseEntity<>(faled(1003,
				httpMessageNotReadableException.getMessage()+"  发送过来的http我读取不了"),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
	/**
	 * 处理 找不到数据的异常或者数据不存在
	 * @param notFoundException notFoundException
	 * @return 具体异常信息
	 */
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<UserOV> handlerNotFoundException(NotFoundException notFoundException) {
		return new ResponseEntity<>(faled(1004,
				notFoundException.getMessage() +"   你要找的数据我没有"),
				HttpStatus.NOT_FOUND);
	}
	/**
	 * 处理 空指针异常
	 * @param nullPointerException 空指针异常
	 * @return 具体异常信息
	 */
	@ExceptionHandler(NullPointerException.class)
	public ResponseEntity<UserOV> handlerNullPointerException(NullPointerException nullPointerException){
		nullPointerException.getMessage();
		return new ResponseEntity<>(faled(1013,
				nullPointerException.getMessage()+"  空指针异常"),
				HttpStatus.BAD_REQUEST);
	}

	/**
	 * 处理 登陆失败
	 * @return 具体异常信息
	 */
	@ExceptionHandler(LoginException.class)
	public ResponseEntity<UserOV> handlerNullPointerException(LoginException l){
		l.getMessage();
		return new ResponseEntity<>(faled(1013,
				l.getMessage()+"登陆失败"),
				HttpStatus.BAD_REQUEST);
	}
	/**
	 * 处理 token找不到或者过期
	 * @return 具体异常信息
	 */
	@ExceptionHandler(PastException.class)
	public ResponseEntity<UserOV> handlerNullPointerException(PastException l){
		l.getMessage();
		return new ResponseEntity<>(faled(1013,
				l.getMessage()+"登录过期"),
				HttpStatus.BAD_REQUEST);
	}
}
