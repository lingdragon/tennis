package com.tidc.authorization.exception;

/**
 * @ClassNmae LoginException
 * @Description TODO
 * @Author 冯涛滔
 **/

public class LoginException extends Exception{
	private String name;
	public LoginException(String name){
		this.name = name;
	}
	@Override
	public String getMessage() {
		return name+"用户名密码错误";
	}
}
