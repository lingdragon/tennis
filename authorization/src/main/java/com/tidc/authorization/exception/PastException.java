package com.tidc.authorization.exception;

/**
 * @ClassNmae PastException
 * @Description TODO
 * @Author 冯涛滔
 **/
public class PastException extends Exception{
	private String name;
	public PastException(String name){
		this.name = name;
	}
	@Override
	public String getMessage() {
		return name;
	}
}
