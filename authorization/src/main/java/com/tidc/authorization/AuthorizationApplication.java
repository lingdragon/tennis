package com.tidc.authorization;

import com.tidc.authorization.ov.UserOV;
import com.tidc.authorization.properties.AuthConfigProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;

import javax.sql.DataSource;

@SpringBootApplication
@EnableFeignClients
@EnableConfigurationProperties(AuthConfigProperties.class)//这是让SecurityProperties这个类生效
public class AuthorizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthorizationApplication.class, args);
	}

	//这一段不能注释
	@Primary
	@Bean
	public ClientDetailsService clientDetails(DataSource dataSource) {
		return new JdbcClientDetailsService(dataSource);
	}
	@Bean
	public PasswordEncoder PasswordEncoder(){
		return new BCryptPasswordEncoder();
	}

}
