package com.tidc.authorization.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassNmae LogoutSuccessHandler
 * @Description TODO
 * @Author 14631
 **/
@Component
public class TidcLogoutSuccessHandler implements LogoutSuccessHandler {
	private Logger logger = LoggerFactory.getLogger(TidcLogoutSuccessHandler.class);
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	@Qualifier("consumerTokenServices")
	ConsumerTokenServices consumerTokenServices;
	@Override
	public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
		logger.info("退出成功处理器");
		Map<String, Object> map = new HashMap<>();
		if(consumerTokenServices.revokeToken(httpServletRequest.getParameter("access_token"))){
			map.put("code","200");
			map.put("message","退出成功");
//		如果这个登录的配置返回方式是Json就使用我们自己写的返回Json的方式
			httpServletResponse.setContentType("application/json;charset=UTF-8");
			httpServletResponse.getWriter().write(objectMapper.writeValueAsString(map));
		}else{
			map.put("code","401");
			map.put("message","退出失败");
		}

		}
}
