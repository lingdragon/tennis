package com.tidc.authorization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tidc.authorization.pojo.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassNmae UserMapper
 * @Description TODO
 * @Author 冯涛滔
 **/
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
