package com.tidc.authorization.controller;

import com.tidc.authorization.constant.Status;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassNmae RevokeTokenEndpoint
 * @Description TODO 退出登录
 * @Author 冯涛滔
 **/
@FrameworkEndpoint
public class LogoutController {

	@Autowired
	private ConsumerTokenServices consumerTokenServices;
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	@ResponseBody
	public Map logoutPage (@Param("access_token") String token, OAuth2Authentication oAuth2Authentication) {
		System.out.println(oAuth2Authentication);
		Map result = new HashMap(16);
		if (consumerTokenServices.revokeToken(token)){
			result.put("status", Status.SUCCESS);
			result.put("data", 1);
		} else {
			result.put("status", Status.FAILED);
			result.put("data", 1);
			result.put("code", 1008);
		}

		return result;
	}


}