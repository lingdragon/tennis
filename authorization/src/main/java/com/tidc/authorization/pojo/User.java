package com.tidc.authorization.pojo;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @ClassNmae User
 * @Description TODO
 * @Author 冯涛滔
 **/
@Data
@Component
@Scope(value = "prototype")

public class User implements Serializable {
	private int id;
	private String telephone;
	private String number;
	private String name;
	private String gender;
	private String email;
	private String password;
	private String organization;
}
