package com.tidc.authorization.ov;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @ClassNmae UserOV
 * @Description TODO
 * @Author 冯涛滔
 **/
@Scope(value = "prototype")
@Component
@Accessors(chain = true) //这个注解可以使这个类的set方法返回当前对象
@Data
public class UserOV {
	private int code;
	private String message;
	private Object data;
	public UserOV() {
	}

	public UserOV(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public UserOV(int code, String message, Object data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}
}
